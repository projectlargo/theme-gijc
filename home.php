<?php
/**
 * The homepage template
 */
get_header();

/*
 * Collect post IDs in each loop so we can avoid duplicating posts
 * and get the theme option to determine if this is a two column or three column layout
 */
$ids = array();
?>

<div id="content" class="stories span8" role="main">

	<?php get_template_part( 'home-part', 'bottom-widget-area' ); ?>

	<?php $img_dir = get_stylesheet_directory_uri() . '/img'; ?>
	<aside id="sponsors" class="row-fluid widget">
		<h3 class="widgettitle">Sponsors</h3>
		<h4>Global Investigative Journalism Conference</h4>
		<div class="row row2">
			<a href="http://www.opensocietyfoundations.org/" target="_blank"><img src="<?php echo $img_dir; ?>/osf.jpg" /></a>
			<a href="http://www.regjeringen.no/en/dep/ud.html?id=833" target="_blank"><img src="<?php echo $img_dir; ?>/norwegian.jpg" /></a>
		</div>
		<div class="row row2">
			<a href="http://www.google.com" target="_blank"><img src="<?php echo $img_dir; ?>/google.jpg" /></a>
		</div>
		<div class="row row2">
			<a href="http://www.aljazeera.com/" target="_blank"><img src="<?php echo $img_dir; ?>/aljazeera.jpg" /></a>
			<a href="http://www.adessium.org/" target="_blank"><img src="<?php echo $img_dir; ?>/adessium.jpg" /></a>
		</div>
		<div class="row row6">
			<a href="http://www.mozilla.org/en-US/" target="_blank"><img src="<?php echo $img_dir; ?>/mozilla.jpg" /></a>
			<a href="http://ire.org/" target="_blank"><img src="<?php echo $img_dir; ?>/ire.jpg" /></a>
			<a href="http://www.icfj.org/" target="_blank"><img src="<?php echo $img_dir; ?>/icfj.jpg" /></a>
			<a href="http://www.swissinvestigation.net/en/home/" target="_blank"><img src="<?php echo $img_dir; ?>/swiss.jpg" /></a>
			<a href="http://investigativenewsnetwork.org/" target="_blank"><img src="<?php echo $img_dir; ?>/inn.jpg" /></a>
			<a href="http://dartcenter.org/" target="_blank"><img src="<?php echo $img_dir; ?>/dart.jpg" /></a>
		</div>
		<div class="row row6">
			<a href="https://www.freepressunlimited.org/" target="_blank"><img src="<?php echo $img_dir; ?>/freepress.jpg" /></a>
			<a href="http://www.fairreporters.org/" target="_blank"><img src="<?php echo $img_dir; ?>/fair.jpg" /></a>
			<a href="http://www.icij.org/" target="_blank"><img src="<?php echo $img_dir; ?>/icij.jpg" /></a>
			<a href="http://www.irrp.org.ua/" target="_blank"><img src="<?php echo $img_dir; ?>/regionalpressdevelopment.jpg" /></a>
			<a href="https://reportingproject.net/occrp/" target="_blank"><img src="<?php echo $img_dir; ?>/occrp.jpg" /></a>
			<a href="http://www.media.illinois.edu/knight/" target="_blank"><img src="<?php echo $img_dir; ?>/knightillionois.jpeg" /></a>
		</div>
		<div class="row row6">
			<a href="http://www.fritt-ord.no/" target="_blank"><img src="<?php echo $img_dir; ?>/fritt.jpg" /></a>
			<a href="http://www.transparency.org/" target="_blank"><img src="<?php echo $img_dir; ?>/transparency.jpg" /></a>
			<a href="http://africanmediainitiative.org/" target="_blank"><img src="<?php echo $img_dir; ?>/ami.jpg" /></a>
			<a href="http://www.skup.no/" target="_blank"><img src="<?php echo $img_dir; ?>/skup.jpg" /></a>
			<a href="http://www.nj.no/no/English/" target="_blank"><img src="<?php echo $img_dir; ?>/norwegianunion.jpg" /></a>
		</div>
		<h4>Abraji International Congress</h4>
		<div class="row row2">
			<a href="http://redeglobo.globo.com/" target="_blank"><img src="<?php echo $img_dir; ?>/globo.jpg" /></a>
			<a href="http://www.vale.com/EN/Pages/Landing.aspx" target="_blank"><img src="<?php echo $img_dir; ?>/vale.jpg" /></a>
		</div>
		<div class="row row2">
			<a href="http://www.google.com" target="_blank"><img src="<?php echo $img_dir; ?>/google.jpg" /></a>
		</div>
		<div class="row row6">
			<a href="http://www.embraer.com/" target="_blank"><img src="<?php echo $img_dir; ?>/embraer.jpg" /></a>
			<a href="http://www.folha.uol.com.br/" target="_blank"><img src="<?php echo $img_dir; ?>/folha.jpg" /></a>
			<a href="www.�oi.�com.�br" target="_blank"><img src="<?php echo $img_dir; ?>/oi.jpg" /></a>
			<a href="http://www.uol.com.br/" target="_blank"><img src="<?php echo $img_dir; ?>/uol.jpg" /></a>
			<a href="http://www.odebrecht.com.br" target="_blank"><img src="<?php echo $img_dir; ?>/odebrecht.jpg" /></a>
		</div>
		<h4>Latin American Investigative Journalism Conference (COLPIN)</h4>
		<div class="row row2">
			<a href="http://www.opensocietyfoundations.org/" target="_blank"><img src="<?php echo $img_dir; ?>/osf.jpg" /></a>
			<a href="http://www.fordfoundation.org/" target="_blank"><img src="<?php echo $img_dir; ?>/ford.jpg" /></a>
		</div>
		<div class="row row2">
		</div>

	</aside>
</div><!-- #content-->

<?php get_sidebar(); ?>
<?php get_footer(); ?>